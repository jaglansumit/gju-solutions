package com.sumit.recyclerview;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    Context context;
    ArrayList<Profile> profiles;
    public MyAdapter(Context c , ArrayList<Profile> p)
    {
        this.profiles =p;
        this.context =c;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
         return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {
        final Profile item=profiles.get(i);
     myViewHolder.name.setText(profiles.get(i).getName());
     myViewHolder.email.setText(profiles.get(i).getEmail());

     myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
           //  Intent i=new Intent(context,PdfViewer.class);
            // i.putExtra("pdfurl",item.getEmail());
            // context.startActivity(i);

             Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getEmail()));
             context.startActivity(browserIntent);
         }
     });

    }

    @Override
    public int getItemCount() {
        return profiles.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
      TextView name,email;
      LinearLayout layout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            layout=itemView.findViewById(R.id.layout);
            name=itemView.findViewById(R.id.name);
            email=itemView.findViewById(R.id.email);
        }
    }
}
