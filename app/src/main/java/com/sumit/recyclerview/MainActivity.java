package com.sumit.recyclerview;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private AdView mmmAdview;
    ProgressDialog progressDialog;
    DatabaseReference reference;
    RecyclerView recyclerView;
    ArrayList<Profile> list;
    MyAdapter adapter;
    String Profiles ="Profiles";
   // Button bn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView=findViewById(R.id.myRecyclerView);

        mmmAdview = findViewById(R.id.adddView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mmmAdview.loadAd(adRequest);
       // bn=findViewById(R.id.btn);




        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        list=new ArrayList<Profile>();

        progressDialog=new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Loading! Please wait..");
        progressDialog.show();



        Intent intent = getIntent();
        String department = intent.getStringExtra("dept");
        String semester = intent.getStringExtra("sem");
        String category = intent.getStringExtra("choose");

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference(department+"/"+semester+"/"+category);


        // reference= FirebaseDatabase.getInstance().getReference().child("Profiles");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    Profile p= dataSnapshot1.getValue(Profile.class);
                    list.add(p);
                   // Toast.makeText(MainActivity.this,"Oops! firstly....",Toast.LENGTH_SHORT).show();

                }
                adapter=new MyAdapter(MainActivity.this,list);
                recyclerView.setAdapter(adapter);
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(MainActivity.this,"Oops! Something is wrong....",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
