package com.sumit.recyclerview;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class contribute extends AppCompatActivity {

    private AdView adVieww;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contribute);

        adVieww = findViewById(R.id.contributeAd);
        AdRequest adRequest = new AdRequest.Builder().build();
        adVieww.loadAd(adRequest);

        imageView=findViewById(R.id.gmaill);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:gjusolutions@gmail.com"));
                startActivity(emailIntent);
            }
        });
    }
}
