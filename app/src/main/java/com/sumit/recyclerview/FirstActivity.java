package com.sumit.recyclerview;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class FirstActivity extends AppCompatActivity {
    private AdView mmAdview;
Button btn;
Spinner dept,sem,chs,spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        mmAdview = findViewById(R.id.addView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mmAdview.loadAd(adRequest);

        btn=findViewById(R.id.click);
        dept=findViewById(R.id.dept);
        sem=findViewById(R.id.semester);
        chs=findViewById(R.id.choose);

       // dept.setPrompt("Enter here");

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String department=dept.getSelectedItem().toString();
                String semester=sem.getSelectedItem().toString();
                String choosee=chs.getSelectedItem().toString();
                if(department.equals("Choose Department") || semester.equals("Choose Semester") || choosee.equals("Select Category")){
                    Toast.makeText(FirstActivity.this,"Fill all details:",Toast.LENGTH_SHORT).show();
                }
                else {
                    if(choosee.equals("Notes")){
                       Intent notes =new Intent(FirstActivity.this,contribute.class);
                       startActivity(notes);
                    }
                    else {
                        Intent intent = new Intent(FirstActivity.this, MainActivity.class);
                        intent.putExtra("dept", department);
                        intent.putExtra("sem", semester);
                        intent.putExtra("choose", choosee);
                        startActivity(intent);
                    }
                }
            }
        });
    }
}
